package PT2019.assigment1.Assigment1Project;

import org.junit.Assert;

import Components.Operations;
import Components.Polynom;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	// -----ADDITION-----
	public void testApp1() {
		Polynom p1 = new Polynom();
		Polynom p2 = new Polynom();
		Polynom result = new Polynom();
		Operations op = new Operations();
		p1 = op.pars("3x^2+5x^1");
		p2 = op.pars("2x^1+4x^0");
		result = op.Add(p1, p2);
		Assert.assertTrue(result.toString().equals("3.0x^2+7.0x^1+4.0"));
	}

	// -----SUBTRACTION-----
	public void testApp2() {
		Polynom p1 = new Polynom();
		Polynom p2 = new Polynom();
		Polynom result = new Polynom();
		Operations op = new Operations();
		p1 = op.pars("3x^2+5x^1");
		p2 = op.pars("2x^1+4x^0");
		result = op.Diff(p1, p2);
		Assert.assertTrue(result.toString().equals("3.0x^2+3.0x^1-4.0"));

	}

	// -----MULTIPLICATION-----
	public void testApp3() {
		Polynom p1 = new Polynom();
		Polynom p2 = new Polynom();
		Polynom result = new Polynom();
		Operations op = new Operations();
		p1 = op.pars("3x^2+5x^1");
		p2 = op.pars("2x^1+4x^0");
		result = op.Mul(p1, p2);
		Assert.assertTrue(result.toString().equals("6.0x^3+22.0x^2+20.0x^1"));

	}

	// -----DERIVATION-----
	public void testApp4() {
		Polynom p1 = new Polynom();
		Polynom result = new Polynom();
		Operations op = new Operations();
		p1 = op.pars("3x^2+5x^1");
		result = op.Deriv(p1);
		Assert.assertTrue(result.toString().equals("6.0x^1+5.0"));

	}

	// -----DERIVATION-----
	public void testApp5() {
		Polynom p1 = new Polynom();
		Polynom result = new Polynom();
		Operations op = new Operations();
		p1 = op.pars("3x^2+5x^1");
		result = op.Integ(p1);
		Assert.assertTrue(result.toString().equals("1.0x^3+2.5x^2"));

	}

}
