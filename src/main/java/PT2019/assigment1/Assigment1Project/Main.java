package PT2019.assigment1.Assigment1Project;

import javax.swing.UIManager;
import Components.Monom;
import Components.Operations;
import Components.Polynom;
import View.view;

public class Main {
	private view view;

	public void start() throws Exception {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		view = new view();
		view.setVisible(true);
		initializeButtonListeners();

	}

	private void initializeButtonListeners() throws Exception {

		view.Op1ButtonActionListener(e -> {
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();
			Operations op = new Operations();
			a = op.pars(view.getFirstPol());
			b = op.pars(view.getSecondPol());
			c = op.Add(a, b);
			view.setResult(c.toString());
		});

		view.Op2ButtonActionListener(e -> {
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();
			Operations op = new Operations();
			a = op.pars(view.getFirstPol());
			b = op.pars(view.getSecondPol());
			c = op.Diff(a, b);
			view.setResult(c.toString());
		});

		view.Op3ButtonActionListener(e -> {
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Polynom c = new Polynom();
			Operations op = new Operations();
			a = op.pars(view.getFirstPol());
			b = op.pars(view.getSecondPol());
			c = op.Mul(a, b);
			view.setResult(c.toString());
		});

		view.Op5ButtonActionListener(e -> {
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Operations op = new Operations();
			a = op.pars(view.getFirstPol());
			b = op.Deriv(a);
			view.setSecondPol(" ");
			view.setResult(b.toString());
		});

		view.Op6ButtonActionListener(e -> {
			Polynom a = new Polynom();
			Polynom b = new Polynom();
			Operations op = new Operations();
			a = op.pars(view.getFirstPol());
			b = op.Integ(a);
			view.setSecondPol(" ");
			view.setResult(b.toString() + "+c");
		});
	}
}
