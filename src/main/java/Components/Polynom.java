package Components;

import Components.Monom;
import java.util.*;

public class Polynom {
	ArrayList<Monom> pol = new ArrayList<Monom>();

	public ArrayList<Monom> getPol() {
		return pol;
	}

	public void setPol(ArrayList<Monom> pol) {
		this.pol = pol;
	}

	public String toString() {
		String polynom = "";
		int start =1;
		for (Monom m : pol) {
			if(start==1) {
				polynom = polynom + m.toString();
				start=0;
			}else if (m.getCoeff() != 0.0) {
				if (m.Negative() == true)
					polynom = polynom + m.toString();
				else
					polynom = polynom + "+" + m.toString();
		}
		
		}
		return polynom;
	}

	public void reduce() {
		for (Monom m : pol) {
			if (m.getCoeff() == 0.0) {
				pol.remove(m);
			}
		}
	}

	public void initializePolynom () {
		for (int i = 0; i < pol.size() - 1; i++) {
			for (int j = i + 1; j < pol.size(); j++) {
				if (pol.get(i).getDegree() == pol.get(j).getDegree()) {
					pol.get(i).setCoeff(pol.get(i).getCoeff() + pol.get(j).getCoeff());
					pol.remove(j);
				}
			}
		}
	}

}
