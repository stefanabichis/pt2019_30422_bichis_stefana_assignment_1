package Components;

public class Monom {

	int degree;
	float coeff;

	public Monom(int a, float b) {
		this.degree = a;
		this.coeff = b;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public float getCoeff() {
		return coeff;
	}

	public void setCoeff(float coeff) {
		this.coeff = coeff;
	}

	public boolean Negative() {
		if (getCoeff() < 0)
			return true;
		else
			return false;
	}

	public String toString() {
		if (degree > 0) {
			return Float.toString(coeff) + "x^" + Integer.toString(degree);
		} else {
			return Float.toString(coeff);
		}
	}

}
