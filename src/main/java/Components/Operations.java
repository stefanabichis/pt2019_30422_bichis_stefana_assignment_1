package Components;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Operations {

	public static Polynom pars(String a) {
		Polynom pol = new Polynom();
		final String regex = "([+-]?[^-+]+)";
		final Pattern pattern = Pattern.compile(regex);
		final Matcher matcher = pattern.matcher(a);

		while (matcher.find()) {
			System.out.println(matcher.group(1));
			String[] str = matcher.group(1).split("[x][^\\w]");
			float coeff = Float.valueOf(str[0]);
			int degree = Integer.valueOf(str[1]);
			Monom m = new Monom(degree, coeff);
			pol.getPol().add(m);

		}
		return pol;
	}

// ------ADDITION-----
	public static Polynom Add(Polynom a, Polynom b) {
		Polynom result = new Polynom();
		a.initializePolynom();
		b.initializePolynom();
		result = a;
		int ok = 0;
		for (Monom m : b.getPol()) {
			ok = 0;
			for (Monom n : result.getPol()) {
				if (m.getDegree() == n.getDegree()) {
					n.setCoeff(n.getCoeff() + m.getCoeff());
					ok = 1;
				}
			}
			if (ok == 0) {
				result.getPol().add(m);
			}
		}
		result.initializePolynom();
		result.reduce();

		return result;
	}

// -----SUBSTRACTION-----
	public static Polynom Diff(Polynom a, Polynom b) {
		Polynom result = new Polynom();
		Polynom c = new Polynom();
		for (Monom m : b.getPol()) {
			Monom p = new Monom(m.getDegree(), m.getCoeff() * (-1));
			c.getPol().add(p);
		}
		result = Add(a, c);
		return result;
	}

// -----MULTIPLICATION-----
	public static Polynom Mul(Polynom a, Polynom b) {
		Polynom result = new Polynom();
		for (Monom m : a.getPol()) {
			for (Monom n : b.getPol()) {
				Monom c = new Monom(m.getDegree() + n.getDegree(), m.getCoeff() * n.getCoeff());
				result.getPol().add(c);
			}
		}
		result.initializePolynom();
		result.reduce();

		return result;
	}

// -----DIVISION-----
	/*
	 * public static Polynom Div(Polynom a, Polynom b) { Polynom result = new
	 * Polynom(); Polynom residue = new Polynom(); residue = p1' Polynom(); for
	 * (Monom m : a.getPol()) { for (Monom n : b.getPol()) {
	 * 
	 * } } return result; }
	 */

// -----DERIVATION-----
	public static Polynom Deriv(Polynom a) {
		Polynom result = new Polynom();
		for (Monom m : a.getPol()) {
			if (m.getDegree() > 0) {
				Monom b = new Monom(m.getDegree() - 1, m.getCoeff() * m.getDegree());
				result.getPol().add(b);
			} else {
				Monom b = new Monom(m.getDegree(), 0);
				result.getPol().add(b);
			}
		}
		result.initializePolynom();
		return result;
	}

// -----INTEGRATION-----
	public static Polynom Integ(Polynom a) {
		Polynom result = new Polynom();
		for (Monom m : a.getPol()) {
			Monom b = new Monom(m.getDegree() + 1, m.getCoeff() / (m.getDegree() + 1));
			result.getPol().add(b);
		}
		return result;
	}

}
