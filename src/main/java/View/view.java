package View;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

public class view extends JFrame {

	private JButton op1;
	private JButton op2;
	private JButton op3;
	private JButton op4;
	private JButton op5;
	private JButton op6;
	private JTextField firstPol;
	private JTextField secondPol;
	private JTextField resultText;

	public view() {
		this.setBounds(255, 255, 800, 600);
		getContentPane().setLayout(null);

		JLabel a = new JLabel("Operations with polynomials");
		a.setFont(new Font("Tahoma", Font.PLAIN, 30));
		a.setBounds(210, 11, 400, 39);
		getContentPane().add(a);

		JLabel format = new JLabel("Format of polynomials: coeffx^degree+coeffx^degree-...");
		format.setFont(new Font("Tahoma", Font.PLAIN, 20));
		format.setBounds(10, 81, 700, 39);
		getContentPane().add(format);

		JLabel firstPolynom = new JLabel("First Polynom");
		firstPolynom.setFont(new Font("Tahoma", Font.PLAIN, 20));
		firstPolynom.setBounds(10, 151, 203, 39);
		getContentPane().add(firstPolynom);
		firstPol = new JTextField();
		firstPol.setBounds(200, 151, 203, 39);
		getContentPane().add(firstPol);

		JLabel secondPolynom = new JLabel("Second Polynom");
		secondPolynom.setFont(new Font("Tahoma", Font.PLAIN, 20));
		secondPolynom.setBounds(10, 221, 203, 39);
		getContentPane().add(secondPolynom);
		secondPol = new JTextField();
		secondPol.setBounds(200, 221, 203, 39);
		getContentPane().add(secondPol);

		JLabel result = new JLabel("Result");
		result.setFont(new Font("Tahoma", Font.PLAIN, 20));
		result.setBounds(10, 291, 203, 39);
		getContentPane().add(result);
		resultText = new JTextField();
		resultText.setBounds(200, 291, 400, 39);
		getContentPane().add(resultText);

		op1 = new JButton("+");
		op1.setBounds(30, 431, 120, 25);
		getContentPane().add(op1);

		op2 = new JButton("-");
		op2.setBounds(150, 431, 120, 25);
		getContentPane().add(op2);

		op3 = new JButton("*");
		op3.setBounds(270, 431, 120, 25);
		getContentPane().add(op3);

		op4 = new JButton("/");
		op4.setBounds(390, 431, 120, 25);
		getContentPane().add(op4);

		op5 = new JButton("Derivation");
		op5.setBounds(510, 431, 120, 25);
		getContentPane().add(op5);

		op6 = new JButton("Integration");
		op6.setBounds(630, 431, 120, 25);
		getContentPane().add(op6);

	}

	public void Op1ButtonActionListener(final ActionListener actionListener) {
		op1.addActionListener(actionListener);
	}

	public void Op2ButtonActionListener(final ActionListener actionListener) {
		op2.addActionListener(actionListener);
	}

	public void Op3ButtonActionListener(final ActionListener actionListener) {
		op3.addActionListener(actionListener);
	}

	public void Op4ButtonActionListener(final ActionListener actionListener) {
		op4.addActionListener(actionListener);
	}

	public void Op5ButtonActionListener(final ActionListener actionListener) {
		op5.addActionListener(actionListener);
	}

	public void Op6ButtonActionListener(final ActionListener actionListener) {
		op6.addActionListener(actionListener);
	}

	public String getFirstPol() {
		return firstPol.getText();
	}

	public String getSecondPol() {
		return secondPol.getText();
	}

	public void setSecondPol(String secondPol) {
		this.secondPol.setText(secondPol);
	}

	public void setResult(String textField) {
		this.resultText.setText(textField);
	}

}
